#!/bin/bash
jpegoptim -m 60 -p -d ./images ./orig/*.JPG
if [[ -e ./orig/darktable_exported ]]; then
  jpegoptim -m 60 -p -d ./images ./orig/darktable_exported/*.jpg
fi
